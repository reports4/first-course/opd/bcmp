; z = 2^(-f), x=x1*z
; n=16, f=12 - дробная, i=4 - целая 
org 009
index:          word 0000               ; индекная ячейка для констант

org 010                                 ; данные
argF1:          word 0000               ; первый аргумент в функции
argF2:          word 0000               ; второй агрумент в функции
past_bit:       word 0000               ; прошлое состояние бита
new_bit:        word 0000               ; новое состояние бита
temp:           word 0000               ; временная переменная
last_bit:       word 0001  ;const       ; выделение 0 бита
shift_and15:    word 8000  ;const       ; выделение 15 бита
shift_and14:    word 4000  ;const       ; выделение 14 бита
F_shift:        word 0000               ; последний сдвиг
F_shift_sv:     word FFF4  ;const       ; сохраненный сдвиг
repeat:         word 0000               ; итоговое значение счетчика
repeat_sv:      word fff0  ;const       ; значение счетчика
scp_up:         word 0000               ; СЧП старшие разряды/делимое-деитель
scp_down:       word 0000               ; СЧП младшые разряды/делитель-остаток

double_x:       word 0000               ; квадрат введенного аргумента
temp_pow_x:     word 0000
lenght_const:   word 0003  ;const       ; длинна массива констант
counter_lengh:  word 0000               ; для счетчика
start_addr_c:   word wrd_2 ;const       ; начальный адрес 1 элемента

;wrd_1:          word 1000               ; массив констант для деления
wrd_2:          word 2000  ;const
wrd_3:          word 3000  ;const
wrd_4:          word 4000  ;const
wrd_5:          word 5000  ;const
wrd_6:          word 6000  ;const
wrd_7:          word 7000  ;const

angle:          word 1000               ; введенный угол x1
R:              word 0000               ; результат

org 100                                 ; сама прогамма
begin:          cla
                mov R                   ; обнуляем все ячейки
                mov double_x
                mov index
                
                add lenght_const        ; зарузка счетчика
                cma
                inc
                mov counter_lengh
                
                cla
                add start_addr_c        ; загрузка адреса первого элемента
                mov index

		cla
                add angle
                beq exit
                bmi exit

                mov argF1                ; получение квадрата, для упрощения
                mov argF2
                jsr func_mul
                mov double_x

                cla                      ; певрый член
                add angle
                mov R
                mov temp_pow_x

next_sin:       cla
                add temp_pow_x
                mov argF1                ; i член
                cla
                add double_x
                mov argF2
                jsr func_mul
                mov argF1
                cla
                add (index)
                mov argF2
                jsr func_del
                mov argF1
                cla
                add (index)
                mov argF2
                jsr func_del
                mov temp_pow_x          ; сохранение x^i/i!
                
                cla
                add counter_lengh
                ror
                bcs sb
                cla
                add R
                add temp_pow_x
                mov R
                br loop
sb:             cla
                add R
                sub temp_pow_x
                mov R

loop:           isz counter_lengh
                br next_sin
exit:           hlt

org 600
func_mul:       word 0000               ; функция умножения без коррекции с последующим сдвигом
                cla
                mov past_bit
                mov scp_up            
                add repeat_sv
                mov repeat
                cla
                add F_shift_sv
                mov F_shift
                cla
                add argF2
                mov scp_down
next:           cla
                add scp_down       
                and last_bit            ; выделяю последний бит
                mov temp                ; сохраняю его
                add past_bit        
                beq shift               ; если нет изменения бита
                sub last_bit
                beq analiz              ; есть изменение бита
                br shift                ; нет изменеия бита
analiz:         cla                     ; следующий анализ что добавлять
                add past_bit
                sub temp
                bpl analizp             ; если с 1 -> 0 складываем 
                cla            
                add scp_up              ; если с 0 -> 1 вычитаем
                sub argF1
                mov scp_up
                br shift
analizp:        cla    
                add scp_up
                add argF1
                mov scp_up
shift:          cla                     ; сдвиг
                add temp
                mov past_bit
                cla
                add scp_down            ; сдвиг младших счп
                ror
                mov scp_down
                cla
                add scp_up              ; сдвиг старших счп
                ror
                mov scp_up
                ror
                and shift_and15
                add scp_down
                mov scp_down
                cla
                add scp_up
                and shift_and14         ; арифметический сдвиг
                rol
                add scp_up
                mov scp_up
                isz repeat
                br next
lst_shtr:       cla                     ; последний сдвиг на двух словах
                add scp_down            ; сдвиг младших счп
                ror
                mov scp_down
                cla
                add scp_up              ; сдвиг старших счп
                ror
                mov scp_up
                ror
                and shift_and15
                add scp_down
                mov scp_down 
                isz F_shift           
                br lst_shtr
                cla
                add scp_down
                br (func_mul)           ; возврат из функции, в аккумуляторе - значение

func_del:       word 0000               ; функция деления
                cla
                mov past_bit            
                add repeat_sv
                mov repeat
                cla
                add F_shift_sv
                mov F_shift
                cla
                mov scp_up
                add argF1
                mov scp_down
            
                cla                     ; сохранение знака делителя
                add argF2
                and shift_and15            
                mov past_bit        

                cla                     ; первый сдвиг
                add scp_up
                rol
                mov scp_up
                cla
                add scp_down
                rol
                mov scp_down
                cla
                adc scp_up
                mov scp_up
            
                sub argF2               ; первое вычитание
                mov scp_up
                and shift_and15         ; выделение знака
                mov new_bit
                sub past_bit
                ;beq exit
                cla
                add repeat
                add last_bit
                mov repeat

del_next:       cla                     ; сдвиг
                add scp_up
                rol
                mov scp_up
                cla
                add scp_down
                rol
                mov scp_down
                cla
                adc scp_up
                mov scp_up
            
                cla
                add new_bit
                sub past_bit
                beq sovp
                cla
                add scp_up
                add argF2
                mov scp_up
                br signsn        
sovp:           cla
                add scp_up
                sub argF2
                mov scp_up
signsn:         and shift_and15
                mov new_bit
                sub past_bit    
                beq form_s
                br next_op

form_s:         cla
                add scp_down
                add last_bit
                mov scp_down

next_op:        isz repeat
                br del_next

lst_sftl:       cla                     ; сдвиг
                add scp_up
                rol
                mov scp_up
                cla
                add scp_down
                rol
                mov scp_down
                cla
                adc scp_up
                mov scp_up
                isz F_shift           
                br lst_sftl
                cla
                add scp_down
                br (func_del)