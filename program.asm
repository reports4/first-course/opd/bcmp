ORG 41D
BEGIN:  CLA
        MOV R
        ADD Z
        JSR FUNC
        SUB R
        MOV R
        CLA
        ADD X
        INC
        JSR FUNC
        INC
        SUB R
        MOV R
        CLA
        ADD Y
        JSR FUNC
        ADD R
        MOV R
        HLT
Z:      WORD 012C
Y:      WORD 012A
X:      WORD 00FA
R:      WORD 0000

ORG 689
FUNC:   WORD 0000
        bpl TWO
        sub RO
        bmi TWO
        beq TWO
        add RO
        mov RES
        add RES
        add RES
        sub NA
        br (FUNC)
TWO:    cla
        add RO
        br (FUNC)
RES:    WORD 0000
RO:     WORD FB1C
NA:     WORD 0085
