org C
ind:    word 0684

org 010
str:    word 0684
count:  word 0000

org 684
len:    word 0000

org 076
begin:  cla
        mov (str)
        add str
        mov ind       
        cla
        jsr readExtD    ; чтение кол-во символов
        add count	; изменение бита Z (sub 0030)
        beq exit        ; проверка на отрицательность
        mov (ind)
        cma
        inc
        mov count       ; создание счетчика для повтора чтение символов

rep:    cla             ; начало цикла чтение
        jsr readExtD    ; запись в старшие 8 бит
        rol
        rol
        rol
        rol
        rol
        rol
        rol
        rol
        isz count       ; привращение и если все символы уже прочитаны, записать в память
        br nn
        br write
nn:     jsr readExtD    ; запись в младшие 8 бит
        mov (ind)
        isz count       ; привращение 
        br rep
        br exit

write:  mov (ind)
exit:   hlt             ;выход из программы

org 100
readExtD:   word 0000   ; Чтение данных из ВУ2
loop:       tsf 2
            br loop
            in 2
            clf 2
            br (readExtD)