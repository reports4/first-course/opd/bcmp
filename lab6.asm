org 000
ret:    word 0000
        nop
        br interrupt

org 010
min:    word FFE0
max:    word 001F
sub_v:  word 0002
add_F:  word 0003
mask:   word 0800
neg:    word ff00

org 02C
X:      word 0000

org 100
begin:  cla
        di
        add X
        sub sub_v
        
        sub min
        bmi chng
        add min
        sub max
        bpl chng
        add max
        br write
        
chng:   cla
        add max
        ei
write:  mov X
        br begin

org 300
save_A:     word 0000
interrupt:  mov save_A
          
I2:         tsf 2
            br I3
            cla
            clf 2
            in 2
            and mask
            beq pl
            cla
            in 2
            add neg
            br check
pl:         in 2
check:      sub min   
            bmi chng2  
            add min   
            sub max   
            bpl chng2  
            add max     
            br writeIO
chng2:      cla
            add max
writeIO:    mov X

IO:         nop
            cla
            add save_A
            ei
            br (ret)              

I3:         tsf 3
            br IF1
            cla
            add X
            rol
            clc
            rol
            clc
            cma
            inc
            sub add_F
            out 3
            clf 3
            br IO

IF1:        tsf 1
            br IF4
            clf 1

IF4:        tsf 4
            br IF5
            clf 4

IF5:        tsf 5
            br IF6
            clf 5

IF6:        tsf 6
            br IF7
            clf 6

IF7:        tsf 7
            br IF8
            clf 7

IF8:        tsf 8
            br IF9
            clf 8
            
IF9:        tsf 9
            br IO
            clf 9
            br IO
                                   
