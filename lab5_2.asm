org C
ind:    word 0000

org 10
str:    word 0684       ;const
count:  word 0000
pos:    word 0000
tmp:    word 0000

org 076
begin:  cla             ; подготовка ячеек памяти
        mov (str)       ;
        mov tmp         ;
        mov pos         ;
        ADD str         ;
        mov ind         ;
        cla             ; чтение кол-во символов
        jsr readExtD    ;
        add tmp         ;
        beq exit        ;
        mov (ind)       ;
        cma             ; создание счетчика для повтора чтение символов
        inc             ;
        mov count       ; 
next:   cla             ; определение, куда надо записывать значение из ВУ2(в старшие 8 бит/младшие 8 бит)
        add pos         ;
        inc             ;
        mov pos         ;
        ror             ;
        bcs sen         ;
jun:    cla             ; чтение символа и запись в младшие 8 бит, запись полученных 2х символов в память
        clc             ;
        jsr readExtD    ;
        add tmp         ;
        mov (ind)       ;
        cla             ;
        mov tmp         ;
        isz count       ;
        br next         ;
        br exit        ;
sen:    cla             ; чтение символа и запись в старшие 8 бит
	    clc             ;
        jsr readExtD    ;
        rol             ;
        rol             ;
        rol             ;
        rol             ;
        rol             ;
        rol             ;
        rol             ;
        rol             ;
        mov tmp         ;
        isz count       ;
        br next         ;
        br write        ;
write:  cla             ; запись остаткочных данных при нечетном кол-во символов
        add tmp         ;
        beq exit        ;
        mov (ind)       ;
exit:   hlt             ; останов

org 100
readExtD:   word 0000   ; Чтение данных из ВУ2
            cla
loop:       TSF 2
            BR loop
            IN 2
            CLF 2
            BR (readExtD)